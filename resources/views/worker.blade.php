<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Customer Care</title>
        <link rel="stylesheet" href="//media.twiliocdn.com/taskrouter/quickstart/agent.css" />
        <script type="text/javascript" src="//media.twiliocdn.com/taskrouter/js/v1.20/taskrouter.min.js"></script>

        <script src="https://7eb3ddf1.ngrok.io/js/agent.js" defer></script>
    </head>
    <body>
        <div class="content">
            <section class="agent-activity offline">
                <p class="activity">Offline</p>
                <button class="change-activity" data-next-activity="Available">Go Available</button>
            </section>
            <section class="agent-activity available">
                <p class="activity">Available</p>
                <button class="change-activity" data-next-activity="Offline">Go Offline</button>
            </section>

            <section class="log">
                <textarea id="log" readonly="true"></textarea>
            </section>

        </div>

        <script>
            window.workerToken="{{ $workerToken }}";
        </script>

    </body>
    </html>