<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $fillable = [
        'from',
        'to',
        'message',
    ];

    protected $table = 'smss';
}
