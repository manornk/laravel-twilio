<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Main view
 */
Route::get(
    '/', function () {
        $missed_calls = App\MissedCall::orderBy('created_at', 'desc')->get();

        $twilioNumber = config('services.twilio')['number']
          or die("TWILIO_NUMBER is not set in the system environment");

        return view(
            'welcome', [
            "missed_calls" => $missed_calls,
            "twilioNumber" => $twilioNumber
            ]
        );
    }
);



Route::get('/user', 'WorkersControllers@show');




/**
 * Endpoints
 */
Route::post(
    '/call/incoming',
    ['uses' => 'IncomingCallController@respondToUser',
        'as' => 'call.incoming']
);
Route::post(
    '/call/enqueue',
    ['uses' => 'EnqueueCallController@enqueueCall',
        'as' => 'call.enqueue']
);
Route::post(
    '/assignment',
    ['uses' => 'CallbackController@assignTask',
        'as' => 'assignment']
);
Route::post(
    '/events',
    ['uses' => 'CallbackController@handleEvent',
        'as' => 'events']
);
Route::post(
    '/message/incoming',
    ['uses' => 'MessageController@handleIncomingMessage',
        'as' => 'messages']
);


Route::get(
    '/message/send',
    ['uses' => 'MessageController@sendSms',
        'as' => 'messages']
);

Route::post(
    '/message/send',
    ['uses' => 'MessageController@sendTwilioSms',
        'as' => 'messages']
);
Route::get(
    '/message',
    ['uses' => 'MessageController@index',
        'as' => 'messages']
);





Route::get('/worker', function () {
    
    $accountSid = "AC6622faafebb175961b66019284f2ba3a";
    $authToken = "c19d17af1d9b5778d5b1c8f59ceb05a8";

    $workspaceSid = env('WORKSPACE_SID');
    // $workspaceSid = "WS4159d50f44eb18f2d9e88f2d5f45332d";

    $workerSid = $_REQUEST["WorkerSid"];

    $workerCapability  =  new Twilio\Jwt\TaskRouter\WorkerCapability(
        $accountSid,
        $authToken,
        $workspaceSid,
        $workerSid
    );
    $workerCapability->allowActivityUpdates();
    $workerToken = $workerCapability->generateToken();

    return view('worker', compact('workerToken'));
});


Route::get('/capability', function() {

    $accountSid = 'AC6622faafebb175961b66019284f2ba3a';
    $authToken  = 'c19d17af1d9b5778d5b1c8f59ceb05a8';
    $appSid = 'APba6eab04d3252a5983daba9252814f9f';
    
    $capability = new \Twilio\Jwt\ClientToken($accountSid, $authToken);
    $capability->allowClientOutgoing($appSid);
    $capability->allowClientIncoming('rajko');
    $token = $capability->generateToken();
    
    echo $token;
});

