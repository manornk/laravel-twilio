<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TaskRouter\WorkspaceFacade;
use App\Exceptions\TaskRouterException;

class WorkersControllers extends Controller
{

    public function show(Request $request, WorkspaceFacade $workspace)
    {
        $phone = $request->get('phone');
        $phone = "+".$phone;
        // $worker = $this->getWorkerByPhone($phone, $workspace);
        $workers = $workspace->workers->read(array(), 20);

        dd($workers);

        return view('worker', compact('worker'));
    }
    

    public function changeWorkerStatus($number, $newWorkerStatus, WorkspaceFacade $workspace) 
    {
        $worker = $this->getWorkerByPhone($number, $workspace);
        $this->updateWorkerStatus($worker, $newWorkerStatus, $workspace);
    }

    function updateWorkerStatus($worker, $status, $workspace)
    {
        $wantedActivity = $workspace->findActivityByName($status);
        $workspace->updateWorkerActivity($worker, $wantedActivity->sid);
    }

    protected function getWorkerByPhone($phone, $workspace)
    {
        $phoneToWorkerStr = config('services.twilio')['phoneToWorker'];
        parse_str($phoneToWorkerStr, $phoneToWorkerArray);
        if (empty($phoneToWorkerArray[$phone])) {
            throw new TaskRouterException("You are not a valid worker");
        }
        return $workspace->findWorkerBySid($phoneToWorkerArray[$phone]);
    }
}

//  function addWorker($params)
    // {
        // return $this->_workspace->workers->create($params['friendlyName'], $params);
    // }
