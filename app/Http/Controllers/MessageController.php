<?php

namespace App\Http\Controllers;

use App\Exceptions\TaskRouterException;
use Illuminate\Http\Request;
use App\TaskRouter\WorkspaceFacade;
use Twilio\Twiml;
use App\Sms;
use Twilio\Rest\Client;

class MessageController extends Controller
{


    public function index()
    {
        $smss = Sms::all();

        return view('messages.index', compact('smss'));
    }

    public function handleIncomingMessage(Request $request, WorkspaceFacade $workspace) 
    {
        $message = strtolower($request->input("Body"));
        $fromNumber = $request->input("From");

        try {
            $data = [
                'from' => $fromNumber,
                'message' => $message
            ];

            Sms::create($data);
        } catch (TaskRouterException $e) {
            // $response->sms($e->getMessage());
        }

        return response($response)->header('Content-Type', 'text/xml');
    }

    public function sendSms()
    {
        return view('messages.send');
    }

    public function sendTwilioSms(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'to' => "required",
            'message' => "required",
            'whatsapp' => "required"
        ]);
        
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->get('whatsapp') == 'yes') {
            $from = 'whatsapp:+14155238886';
            $to = 'whatsapp:' . $request->get('to');
        } else {
            $from = env('TWILIO_NUMBER');
            $to = $request->get('to');
        }

        $data = [
            'from' => $from,
            'to' => $to,
            'message' => $request->get('message')
        ];

        Sms::create($data);

        $sid = "AC6622faafebb175961b66019284f2ba3a";
        $token = "c19d17af1d9b5778d5b1c8f59ceb05a8";

        $twilio = new Client($sid, $token);

        $message = $twilio->messages
                        ->create($to,
                                array(
                                    "body" => $request->get('message'),
                                    "from" => $from, /// . env('TWILIO_NUMBER'),
                                )
                        );

        return back()->with('flash', 'Successfully sent');
    }














    function updateWorkerStatus($worker, $status, $workspace)
    {
        $wantedActivity = $workspace->findActivityByName($status);
        $workspace->updateWorkerActivity($worker, $wantedActivity->sid);
    }

    protected function getWorkerByPhone($phone, $workspace)
    {
        $phoneToWorkerStr = config('services.twilio')['phoneToWorker'];
        parse_str($phoneToWorkerStr, $phoneToWorkerArray);
        if (empty($phoneToWorkerArray[$phone])) {
            throw new TaskRouterException("You are not a valid worker");
        }
        return $workspace->findWorkerBySid($phoneToWorkerArray[$phone]);
    }
}